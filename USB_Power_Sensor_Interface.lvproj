﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="16008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="controls" Type="Folder">
			<Item Name="ctl_PM_Model.ctl" Type="VI" URL="../controls/ctl_PM_Model.ctl"/>
			<Item Name="fgv_Class.ctl" Type="VI" URL="../controls/fgv_Class.ctl"/>
			<Item Name="Macro Queue Action.ctl" Type="VI" URL="../controls/Macro Queue Action.ctl"/>
			<Item Name="Message and Data.ctl" Type="VI" URL="../controls/Message and Data.ctl"/>
			<Item Name="Queue Manager Priority.ctl" Type="VI" URL="../controls/Queue Manager Priority.ctl"/>
			<Item Name="System VISA Control.ctl" Type="VI" URL="../controls/System VISA Control.ctl"/>
			<Item Name="System VISA Resource.ctl" Type="VI" URL="../controls/System VISA Resource.ctl"/>
		</Item>
		<Item Name="icon" Type="Folder">
			<Item Name="U2022X.ico" Type="Document" URL="../icon/U2022X.ico"/>
		</Item>
		<Item Name="instrument" Type="Folder">
			<Item Name="Documents" Type="Folder">
				<Item Name="Project UML.bak" Type="Document" URL="/&lt;instrlib&gt;/instrument/Documents/Project UML.bak"/>
				<Item Name="Project UML.uml" Type="Document" URL="/&lt;instrlib&gt;/instrument/Documents/Project UML.uml"/>
			</Item>
			<Item Name="Package Build" Type="Folder">
				<Item Name="Instrument Drivers.vipb" Type="Document" URL="/&lt;instrlib&gt;/instrument/Package Build/Instrument Drivers.vipb"/>
				<Item Name="qorvo_lib_instrument_drivers-1.0.0.1.vip" Type="Document" URL="/&lt;instrlib&gt;/instrument/Package Build/qorvo_lib_instrument_drivers-1.0.0.1.vip"/>
				<Item Name="qorvo_lib_instrument_drivers-1.0.0.2.vip" Type="Document" URL="/&lt;instrlib&gt;/instrument/Package Build/qorvo_lib_instrument_drivers-1.0.0.2.vip"/>
			</Item>
			<Item Name="Adjust for Cable Resistance.vi" Type="VI" URL="../Instrument Drivers/Instrument/Adjust for Cable Resistance.vi"/>
			<Item Name="Check IDN Match.vi" Type="VI" URL="../Instrument Drivers/Instrument/Check IDN Match.vi"/>
			<Item Name="Close VISA Session.vi" Type="VI" URL="../Instrument Drivers/Instrument/Close VISA Session.vi"/>
			<Item Name="Create WV File Format.vi" Type="VI" URL="../Instrument Drivers/Instrument/Create WV File Format.vi"/>
			<Item Name="dir.mnu" Type="Document" URL="../Instrument Drivers/Instrument/dir.mnu"/>
			<Item Name="Instrument - Read Address.vi" Type="VI" URL="../Instrument Drivers/Instrument/Instrument - Read Address.vi"/>
			<Item Name="Instrument Drivers.lvproj" Type="Document" URL="../Instrument Drivers/Instrument/Instrument Drivers.lvproj"/>
			<Item Name="Instrument.lvclass" Type="LVClass" URL="../Instrument Drivers/Instrument/Instrument.lvclass"/>
			<Item Name="Open VISA Session.vi" Type="VI" URL="../Instrument Drivers/Instrument/Open VISA Session.vi"/>
			<Item Name="Unsupported Function Error.vi" Type="VI" URL="../Instrument Drivers/Instrument/Unsupported Function Error.vi"/>
		</Item>
		<Item Name="Power Meter Base" Type="Folder">
			<Item Name="Power Meter Base.lvclass" Type="LVClass" URL="../Instrument Drivers/Power Meters/Power Meter Base/Power Meter Base.lvclass"/>
		</Item>
		<Item Name="Power Meter Classes" Type="Folder">
			<Item Name="Agilent U200xx" Type="Folder">
				<Item Name="Agilent U200xx.aliases" Type="Document" URL="../Instrument Drivers/Power Meters/Agilent U200xx/Agilent U200xx.aliases"/>
				<Item Name="Agilent U200xx.lvclass" Type="LVClass" URL="../Instrument Drivers/Power Meters/Agilent U200xx/Agilent U200xx.lvclass"/>
				<Item Name="Agilent U200xx.lvlps" Type="Document" URL="../Instrument Drivers/Power Meters/Agilent U200xx/Agilent U200xx.lvlps"/>
				<Item Name="Agilent U200xx.lvproj" Type="Document" URL="../Instrument Drivers/Power Meters/Agilent U200xx/Agilent U200xx.lvproj"/>
				<Item Name="package_info.ini" Type="Document" URL="../Instrument Drivers/Power Meters/Agilent U200xx/package_info.ini"/>
			</Item>
			<Item Name="Agilent U84xx.lvclass" Type="LVClass" URL="../Instrument Drivers/Power Meters/Agilent U84xx_class/Agilent U84xx.lvclass"/>
			<Item Name="Agilent U202xx.lvclass" Type="LVClass" URL="../Instrument Drivers/Power Meters/Agilent U202xx/Agilent U202xx.lvclass"/>
			<Item Name="Keysight U2040 X-Series.lvclass" Type="LVClass" URL="../Instrument Drivers/Instrument/Keysight U2040 X-Series.lvclass"/>
			<Item Name="Rohde &amp; Schwarz NRP.lvclass" Type="LVClass" URL="../Instrument Drivers/Power Meters/Rohde &amp; Schwarz NRP/Rohde &amp; Schwarz NRP.lvclass"/>
		</Item>
		<Item Name="subVIs" Type="Folder">
			<Item Name="fgv_PM_Class.vi" Type="VI" URL="../subVIs/fgv_PM_Class.vi"/>
			<Item Name="fgv_String.vi" Type="VI" URL="../subVIs/fgv_String.vi"/>
			<Item Name="Frequency_Dialog.vi" Type="VI" URL="../subVIs/Frequency_Dialog.vi"/>
			<Item Name="Frequency_Range_String.vi" Type="VI" URL="../subVIs/Frequency_Range_String.vi"/>
			<Item Name="Queue Manager.vi" Type="VI" URL="../subVIs/Queue Manager.vi"/>
			<Item Name="test_class.vi" Type="VI" URL="../subVIs/test_class.vi"/>
			<Item Name="Tick Count (ms)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/time/time.llb/Tick Count (ms)__ogtk.vi"/>
		</Item>
		<Item Name="USB_Power_Sensor_Interface.vi" Type="VI" URL="../USB_Power_Sensor_Interface.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="VISA GPIB Control REN Mode.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA GPIB Control REN Mode.ctl"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="USB_Power_Sensor_Interface" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{32458109-17EA-4C28-9DAE-C9D2D4EF2294}</Property>
				<Property Name="App_INI_GUID" Type="Str">{AFBFC191-17B2-4996-A65C-079951F83EE5}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{BE764E14-96FD-4DCD-9DE9-C7DDB3582617}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">USB_Power_Sensor_Interface</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{262A832E-F6D7-4BD9-8149-9D03B2AD7563}</Property>
				<Property Name="Bld_version.build" Type="Int">6</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">USB Power Meter.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/USB Power Meter.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/icon/U2022X.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{06AB0CD3-88D8-4B86-9514-31D8D9897459}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/USB_Power_Sensor_Interface.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Qorvo</Property>
				<Property Name="TgtF_fileDescription" Type="Str">USB_Power_Sensor_Interface</Property>
				<Property Name="TgtF_internalName" Type="Str">USB_Power_Sensor_Interface</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2019 Qorvo</Property>
				<Property Name="TgtF_productName" Type="Str">USB Power Sensor GUI</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{9FA423FC-2B14-4507-BF22-9D99484146CA}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">USB Power Meter.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
