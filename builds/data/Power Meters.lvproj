﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="16008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Ancestors" Type="Folder">
			<Item Name="Instrument.lvclass" Type="LVClass" URL="../../Instrument/Instrument.lvclass"/>
		</Item>
		<Item Name="Common" Type="Folder">
			<Item Name="Close VISA Session.vi" Type="VI" URL="../../common/Close VISA Session.vi"/>
			<Item Name="Open VISA Session.vi" Type="VI" URL="../../common/Open VISA Session.vi"/>
		</Item>
		<Item Name="Plugins" Type="Folder">
			<Item Name="Implementations" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="Agilent E4416A-7A.lvclass" Type="LVClass" URL="../Agilent E4416A-7A/Agilent E4416A-7A.lvclass"/>
				<Item Name="Agilent E4418B-9B.lvclass" Type="LVClass" URL="../Agilent E4418B-9B/Agilent E4418B-9B.lvclass"/>
				<Item Name="Agilent N1911A-2A.lvclass" Type="LVClass" URL="../Agilent N1911A-2A/Agilent N1911A-2A.lvclass"/>
				<Item Name="Agilent N1913A-4A.lvclass" Type="LVClass" URL="../Agilent N1913A-4A/Agilent N1913A-4A.lvclass"/>
				<Item Name="Agilent U200xx.lvclass" Type="LVClass" URL="../Agilent U200xx/Agilent U200xx.lvclass"/>
				<Item Name="Agilent U202xx.lvclass" Type="LVClass" URL="../Agilent U202xx/Agilent U202xx.lvclass"/>
				<Item Name="Anritsu ML2438A.lvclass" Type="LVClass" URL="../Anritsu ML2438A/Anritsu ML2438A.lvclass"/>
				<Item Name="Rohde &amp; Schwarz NRP.lvclass" Type="LVClass" URL="../Rohde &amp; Schwarz NRP/Rohde &amp; Schwarz NRP.lvclass"/>
				<Item Name="Simulated Power Meter.lvclass" Type="LVClass" URL="../Simulated Power Meter/Simulated Power Meter.lvclass"/>
			</Item>
			<Item Name="Power Meter Base.lvclass" Type="LVClass" URL="../Power Meter Base/Power Meter Base.lvclass"/>
		</Item>
		<Item Name="InstrDrivers.lvlib" Type="Library" URL="../../Non Classes/InstrDrivers.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Tick Count (ms)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/time/time.llb/Tick Count (ms)__ogtk.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VISA GPIB Control REN Mode.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA GPIB Control REN Mode.ctl"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
